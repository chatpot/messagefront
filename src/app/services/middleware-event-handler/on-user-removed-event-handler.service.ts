import { Injectable } from "@angular/core";
import {
  UserRemoteProxyService,
  UserDto
} from "../remote-proxies/user-remote-proxy.service";
import { UserService } from "../data-providers/user.service";

@Injectable({
  providedIn: "root"
})
export class OnUserRemovedEventHandlerService {
  constructor(
    private readonly userRemoteProxyService: UserRemoteProxyService,
    private readonly userService: UserService
  ) {
    this.userRemoteProxyService
      .getDeletedUserSubject()
      .subscribe((userDto: UserDto) => {
        this.userService.removeUser(userDto.id);
      });
  }
}
