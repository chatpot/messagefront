import { TestBed } from '@angular/core/testing';

import { DeleteUserCommandService } from './delete-user-command.service';

describe('DeleteUserCommandService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeleteUserCommandService = TestBed.get(DeleteUserCommandService);
    expect(service).toBeTruthy();
  });
});
