import {Injectable} from "@angular/core";
import {
  MessageRemoteProxyService,
  MessageDto
} from "../remote-proxies/message-remote-proxy.service";
import {
  ConversationService,
  Conversation
} from "../data-providers/conversation.service";
import {ScrollMessageHolderService} from "../scroll-message-holder.service";

@Injectable({providedIn: "root"})
export class OnNewMessageEventHandlerService {
  constructor(
      private readonly messageRemoteProxy: MessageRemoteProxyService,
      private readonly conversationService: ConversationService,
      private readonly scrollMessageHolderService: ScrollMessageHolderService) {
    this.messageRemoteProxy.getNewMessageSubject().subscribe(message => {
      this.appendMessageToConversation(
          this.conversationService.getConversation(message.conversationId),
          message);
    });
  }

  private appendMessageToConversation(
      conversation: Conversation, message: MessageDto): void {
    if (conversation) {
      conversation.messages.push({...message});
      conversation.unreadMessage = conversation.unreadMessage + 1;
    }

    if (this.conversationService.isCurrentConversation(message.conversationId))
      this.scrollMessageHolderService.scrollToBottom();
  }
}
