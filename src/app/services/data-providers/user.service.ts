import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import {
  UserRemoteProxyService,
  UserDto
} from "../remote-proxies/user-remote-proxy.service";
import { flatMap } from "rxjs/operators";

export interface User {
  id: string;
  displayName: string;
  image: string;
}

@Injectable({ providedIn: "root" })
export class UserService {
  users: Array<User> = [];
  currentUser: User;

  constructor(
    private readonly userRemoteProxyService: UserRemoteProxyService
  ) {}

  removeUser(userId: string): void {
    this.users = this.users.filter((user: User) => userId !== user.id);
  }

  updateUserDto(userDto: UserDto): void {
    this.users = this.users.map((user: User) => ({
      ...user,
      ...(user.id === userDto.id ? userDto : {})
    }));
  }

  addUser(userDto: UserDto): void {
    this.users.push({ ...userDto });
  }

  public fetch(): void {
    this.userRemoteProxyService.getUsers().subscribe((users: Array<User>) => {
      this.users = users;
    });
  }

  getUser(id: string): User {
    return this.users.find((user: User) => user.id === id);
  }

  loadProfile(): Observable<User> {
    return this.userRemoteProxyService.refresh().pipe(
      flatMap((userDto: UserDto) => {
        return this.userRemoteProxyService.getUser(userDto.id);
      })
    );
  }

  setCurrentUser(user: User) {
    this.currentUser = user;
  }
}
