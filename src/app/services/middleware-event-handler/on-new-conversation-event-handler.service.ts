import { Injectable } from "@angular/core";
import {
  ConversationRemoteProxyService,
  ConversationDto
} from "../remote-proxies/conversation-remote-proxy.service";
import { UserService } from "../data-providers/user.service";
import { ConversationService } from "../data-providers/conversation.service";

@Injectable({
  providedIn: "root"
})
export class OnNewConversationEventHandlerService {
  constructor(
    private readonly conversationRemoteProxyService: ConversationRemoteProxyService,
    private readonly userService: UserService,
    private readonly conversationService: ConversationService
  ) {
    this.conversationRemoteProxyService
      .getNewConversationSubject()
      .subscribe((conversationDto: ConversationDto) => {
        if (conversationDto.members.includes(this.userService.currentUser.id))
          this.conversationService.addConversation(conversationDto);
      });
  }
}
