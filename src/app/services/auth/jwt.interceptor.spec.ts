import { TestBed } from "@angular/core/testing";

import { JWTInterceptor } from "./jwt.interceptor";

describe("JWTInterceptor", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const interceptor: JWTInterceptor = TestBed.get(JWTInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
