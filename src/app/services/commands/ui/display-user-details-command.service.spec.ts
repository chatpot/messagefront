import { TestBed } from '@angular/core/testing';

import { DisplayUserDetailsCommandService } from './display-user-details-command.service';

describe('DisplayUserDetailsCommandService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DisplayUserDetailsCommandService = TestBed.get(DisplayUserDetailsCommandService);
    expect(service).toBeTruthy();
  });
});
