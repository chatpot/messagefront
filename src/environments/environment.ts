import { RemoteProxyConfiguration } from "src/app/services/remote-proxies/remote-proxy-configuration";
import { SocketIoConfig } from "ngx-socket-io";

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const socketIoConfig: SocketIoConfig = {
  url: "http://localhost:3000/events",
  options: {}
};

const remoteProxyConfiguration: RemoteProxyConfiguration = {
  userEndpoint: "http://localhost:3000/api/users/",
  messageEndpoint: "http://localhost:3000/api/messages/",
  conversationEndpoint: "http://localhost:3000/api/conversations/"
};

const firebaseConfig = {
  apiKey: "AIzaSyCbptICT4caxE1-Dl4DnDQO2ZSGmmSZ2Qg",
  authDomain: "chat-po-t.firebaseapp.com",
  databaseURL: "https://chat-po-t.firebaseio.com",
  projectId: "chat-po-t",
  storageBucket: "chat-po-t.appspot.com",
  messagingSenderId: "505881982873",
  appId: "1:505881982873:web:87c5b8c3f0c0a7f4dec508",
  measurementId: "G-WBVM9YWYJE"
};

export const environment = {
  production: false,
  remoteProxyConfiguration: remoteProxyConfiguration,
  socketIoConfig: socketIoConfig,
  firebaseConfig: firebaseConfig
};
