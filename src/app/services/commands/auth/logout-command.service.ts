import { Injectable } from "@angular/core";
import { JWTService } from "../../auth/jwt.service";
import { Router } from "@angular/router";
import { UserService } from "../../data-providers/user.service";
import { HideBottomSheetCommandService } from "../ui/hide-bottom-sheet-command.service";
import { ConversationService } from "../../data-providers/conversation.service";
import { Socket } from "ngx-socket-io";

@Injectable({
  providedIn: "root"
})
export class LogoutCommandService {
  constructor(
    private jwtService: JWTService,
    private readonly socket: Socket,
    private readonly router: Router,
    private readonly userService: UserService,
    private readonly hideBottomSheetCommandService: HideBottomSheetCommandService,
    private readonly conversationService: ConversationService
  ) {}

  execute(): void {
    this.jwtService.setToken(null);
    this.socket.emit("logout");
    this.router.navigate(["/login"]);
    this.userService.setCurrentUser(undefined);
    this.hideBottomSheetCommandService.execute();
    this.conversationService.conversations = [];
  }
}
