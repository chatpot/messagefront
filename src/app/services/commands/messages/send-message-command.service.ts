import {Injectable} from "@angular/core";
import {
  MessageRemoteProxyService
} from "../../remote-proxies/message-remote-proxy.service";
import {UserService} from "../../data-providers/user.service";
import {ConversationService} from '../../data-providers/conversation.service';

@Injectable({providedIn: "root"})
export class SendMessageCommandService {
  constructor(
      private messageRemoteProxyService: MessageRemoteProxyService,
      private readonly userService: UserService,
      private readonly conversationService: ConversationService) {}

  execute(content: string): void {
    this.messageRemoteProxyService
        .create({
          content: content,
          senderId: this.userService.currentUser.id,
          conversationId: this.conversationService.currentConversation.id
        })
        .subscribe((result: any) => {});
  }
}
