import { Component, OnInit, OnDestroy } from "@angular/core";
import { UserService, User } from "../../services/data-providers/user.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.less"]
})
export class UsersComponent {
  constructor(readonly userService: UserService) {}

  getOtherUsers(): Array<User> {
    return this.userService.users.filter((user: User) => {
      return (
        this.userService.currentUser &&
        user.id !== this.userService.currentUser.id
      );
    });
  }
}
