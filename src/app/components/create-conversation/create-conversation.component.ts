import {Component, OnInit, Input} from "@angular/core";
import {FormControl} from "@angular/forms";
import {MatListOption} from "@angular/material";
import {
  CreateConversationCommandService
} from
    "src/app/services/commands/conversations/create-conversation-command.service";

@Component({
  selector: "app-create-conversation",
  templateUrl: "./create-conversation.component.html",
  styleUrls: ["./create-conversation.component.less"]
})
export class CreateConversationComponent implements OnInit {
  @Input() users: Array<MatListOption>;
  nameControl: FormControl;

  constructor(
      private createConversationCommandService:
          CreateConversationCommandService) {}

  ngOnInit() { this.nameControl = new FormControl(); }

  createConversation(): void {
    if (!this.nameControl.value) return;

    this.createConversationCommandService.execute({
      members: [...this.users.map(user => user.value)],
      name: this.nameControl.value
    });
    this.nameControl.setValue("");
  }
}
