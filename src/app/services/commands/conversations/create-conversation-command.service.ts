import {Injectable} from "@angular/core";
import {
  ConversationRemoteProxyService
} from "../../remote-proxies/conversation-remote-proxy.service";
import {User, UserService} from "../../data-providers/user.service";

export interface CreateConversationCommand {
  name: string;
  members: Array<User>;
}

@Injectable({providedIn: "root"})
export class CreateConversationCommandService {
  constructor(
      private conversationRemoteProxyService: ConversationRemoteProxyService,
      private userService: UserService) {}

  execute(createConversationCommand: CreateConversationCommand): void {
    this.conversationRemoteProxyService
        .create({
          members: [
            ...createConversationCommand.members.map(
                (member: User) => member.id),
            this.userService.currentUser.id
          ],
          name: createConversationCommand.name
        })
        .subscribe((result: any) => {});
  }
}
