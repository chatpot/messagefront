import {TestBed} from '@angular/core/testing';

import {
  PickFirstConversationCommandService
} from './pick-first-conversation-command.service';

describe('PickFirstConversationCommandService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PickFirstConversationCommandService =
        TestBed.get(PickFirstConversationCommandService);
    expect(service).toBeTruthy();
  });
});
