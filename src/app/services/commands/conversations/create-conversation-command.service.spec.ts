import { TestBed } from '@angular/core/testing';

import { CreateConversationCommandService } from './create-conversation-command.service';

describe('CreateConversationCommandService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreateConversationCommandService = TestBed.get(CreateConversationCommandService);
    expect(service).toBeTruthy();
  });
});
