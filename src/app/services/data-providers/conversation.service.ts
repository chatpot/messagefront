import { Injectable } from "@angular/core";
import {
  ConversationRemoteProxyService,
  ConversationDto
} from "../remote-proxies/conversation-remote-proxy.service";
import { MessageDto } from "../remote-proxies/message-remote-proxy.service";

export interface Conversation {
  id: string;
  members: Array<string>;
  name: string;
  messages: Array<MessageDto>;
  image: string;
  unreadMessage: number;
}

@Injectable({ providedIn: "root" })
export class ConversationService {
  conversations: Array<Conversation> = new Array<Conversation>();
  currentConversation: Conversation = undefined;

  constructor(
    private readonly conversationRemoteProxyService: ConversationRemoteProxyService
  ) {}

  private setConversations(conversations: Array<Conversation>) {
    this.conversations = conversations;
    this.setCurrentConversation(
      this.conversations.find((conversation: Conversation) => {
        return this.isCurrentConversation(conversation.id);
      })
    );
  }

  removeConversationId(conversationId: string): void {
    this.setConversations(
      this.conversations.filter(
        (conversation: Conversation) => conversation.id !== conversationId
      )
    );
  }

  updateConversationDto(conversationDto: ConversationDto): void {
    this.setConversations(
      this.conversations.map((conversation: Conversation) => {
        return conversation.id === conversationDto.id
          ? { ...conversation, ...conversationDto }
          : conversation;
      })
    );
  }

  addConversation(conversationDto: ConversationDto): void {
    this.setConversations([
      ...this.conversations,
      { ...conversationDto, unreadMessage: 0, messages: [] }
    ]);
  }

  fetch(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.conversationRemoteProxyService.getConversations().subscribe(
        (conversations: Array<ConversationDto>) => {
          this.setConversations(
            conversations
              ? conversations.map((conversationDto: ConversationDto) => ({
                  ...conversationDto,
                  unreadMessage: 0
                }))
              : new Array<Conversation>()
          );
          resolve();
        },
        error => {
          reject(error);
        }
      );
    });
  }

  getConversation(conversationId: string): Conversation {
    return this.conversations.find(
      conversation => conversation.id === conversationId
    );
  }

  setCurrentConversation(conversation: Conversation) {
    this.currentConversation = conversation;
  }

  isCurrentConversation(conversationId: string) {
    return (
      this.currentConversation && this.currentConversation.id === conversationId
    );
  }
}
