import { TestBed } from '@angular/core/testing';

import { QuitConversationCommandService } from './quit-conversation-command.service';

describe('QuitConversationCommandService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QuitConversationCommandService = TestBed.get(QuitConversationCommandService);
    expect(service).toBeTruthy();
  });
});
