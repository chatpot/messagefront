import { TestBed } from '@angular/core/testing';

import { MiddlewareEventHandlerService } from './middleware-event-handler.service';

describe('MiddlewareEventHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MiddlewareEventHandlerService = TestBed.get(MiddlewareEventHandlerService);
    expect(service).toBeTruthy();
  });
});
