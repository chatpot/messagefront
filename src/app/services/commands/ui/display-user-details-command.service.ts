import {Injectable} from '@angular/core';
import {User} from '../../data-providers/user.service';
import {
  UserDetailsSheetComponent
} from 'src/app/sheets/user-details-sheet/user-details-sheet.component';
import {MatBottomSheet} from '@angular/material';

@Injectable({providedIn: 'root'})
export class DisplayUserDetailsCommandService {
  constructor(private matBottomSheet: MatBottomSheet) {}

  execute(user: User): void {
    this.matBottomSheet.open(UserDetailsSheetComponent, {
      data: {user: user},
    });
  }
}
