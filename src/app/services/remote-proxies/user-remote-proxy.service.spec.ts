import { TestBed } from '@angular/core/testing';

import { UserRemoteProxyService } from './user-remote-proxy.service';

describe('UserRemoteProxyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserRemoteProxyService = TestBed.get(UserRemoteProxyService);
    expect(service).toBeTruthy();
  });
});
