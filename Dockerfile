FROM node AS build-env

WORKDIR /usr/src/app

RUN npm install -g @angular/cli

COPY package*.json ./

RUN npm install

COPY . .

# RUN ng test
RUN ng build --prod


FROM nginx

COPY --from=build-env /usr/src/app/dist/MessageFront /usr/share/nginx/html