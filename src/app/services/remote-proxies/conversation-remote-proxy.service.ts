import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, Subject } from "rxjs";
import { Socket } from "ngx-socket-io";
import { RemoteProxyConfiguration } from "./remote-proxy-configuration";
import { MessageDto } from "./message-remote-proxy.service";

export interface ConversationDto {
  name: string;
  members: Array<string>;
  id: string;
  image: string;
  messages: Array<MessageDto>;
}

export interface CreateConversationRequestDto {
  name: string;
  members: Array<string>;
}

export interface UpdateConversationRequestDto {
  name: string;
  members: string[];
  id: string;
  image: string;
}

@Injectable({ providedIn: "root" })
export class ConversationRemoteProxyService {
  private url: string;

  private newConversationSubject: Subject<ConversationDto> = new Subject<
    ConversationDto
  >();

  private updatedConversationSubject: Subject<ConversationDto> = new Subject<
    ConversationDto
  >();

  private deletedConversationSubject: Subject<ConversationDto> = new Subject<
    ConversationDto
  >();

  private newConversation: Observable<ConversationDto> = this.socket.fromEvent<
    ConversationDto
  >("created.conversation");
  private updatedConversation: Observable<
    ConversationDto
  > = this.socket.fromEvent<ConversationDto>("updated.conversation");
  private deletedConversation: Observable<
    ConversationDto
  > = this.socket.fromEvent<ConversationDto>("deleted.conversation");

  constructor(
    private httpClient: HttpClient,
    private socket: Socket,
    private remoteProxyConfiguration: RemoteProxyConfiguration
  ) {
    this.url = this.remoteProxyConfiguration.conversationEndpoint;
    this.newConversation.subscribe((conversation: ConversationDto) => {
      this.newConversationSubject.next(conversation);
    });
    this.updatedConversation.subscribe((conversation: ConversationDto) => {
      this.updatedConversationSubject.next(conversation);
    });
    this.deletedConversation.subscribe((conversation: ConversationDto) => {
      this.deletedConversationSubject.next(conversation);
    });
  }

  getNewConversationSubject(): Observable<ConversationDto> {
    return this.newConversationSubject.asObservable();
  }

  getUpdatedConversationSubject(): Observable<ConversationDto> {
    return this.updatedConversationSubject.asObservable();
  }

  getDeletedConversationSubject(): Observable<ConversationDto> {
    return this.deletedConversationSubject.asObservable();
  }

  getConversations(): Observable<Array<ConversationDto>> {
    return <Observable<Array<ConversationDto>>>this.httpClient.get(this.url);
  }

  create(
    createConversationRequest: CreateConversationRequestDto
  ): Observable<any> {
    return <Observable<any>>(
      this.httpClient.post(this.url, createConversationRequest)
    );
  }

  leave(conversationId: string): Observable<string> {
    return <Observable<string>>(
      this.httpClient.put(this.url + conversationId + "/leave", {})
    );
  }

  update(
    updateConversationRequestDto: UpdateConversationRequestDto
  ): Observable<string> {
    return <Observable<string>>(
      this.httpClient.put(this.url, updateConversationRequestDto)
    );
  }
}
