import {Component, OnInit, OnDestroy, Input} from "@angular/core";
import {Conversation} from "../../services/data-providers/conversation.service";
import {FormControl} from "@angular/forms";
import {
  QuitConversationCommandService
} from
    "src/app/services/commands/conversations/quit-conversation-command.service";
import {
  UpdateConversationCommandService
} from
    "src/app/services/commands/conversations/update-conversation-command.service";

@Component({
  selector: "app-conversation-details",
  templateUrl: "./conversation-details.component.html",
  styleUrls: ["./conversation-details.component.less"]
})
export class ConversationDetailsComponent implements OnInit,
    OnDestroy {
  @Input() conversation: Conversation;

  nameControl: FormControl = new FormControl();
  imageControl: FormControl = new FormControl();

  constructor(
      private quitConversationCommandService: QuitConversationCommandService,
      private updateConversationCommandService:
          UpdateConversationCommandService) {}

  ngOnInit() {
    if (this.conversation) {
      this.nameControl.setValue(this.conversation.name);
      this.imageControl.setValue(this.conversation.image);
    }
  }

  ngOnDestroy(): void {}

  quitConversation(): void {
    this.quitConversationCommandService.execute(this.conversation.id);
  }

  update(): void {
    this.updateConversationCommandService.execute({
      id: this.conversation.id,
      members: this.conversation.members,
      name: this.nameControl.value,
      image: this.imageControl.value
    });
  }
}
