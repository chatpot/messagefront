import {Injectable} from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from "@angular/router";
import {UserService} from "../data-providers/user.service";

@Injectable({providedIn: "root"})
export class LoginGuard implements CanActivate {
  constructor(private userService: UserService, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot):
      boolean {
    if (state.url !== "/login" && !this.userService.currentUser) {
      this.router.navigate(["/login"]);
      return false;
    }
    return true;
  }
}
