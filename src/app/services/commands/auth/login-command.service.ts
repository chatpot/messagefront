import { Injectable } from "@angular/core";
import { JWTService } from "../../auth/jwt.service";
import { Socket } from "ngx-socket-io";
import { UserService, User } from "../../data-providers/user.service";
import { Router } from "@angular/router";
import { ConversationService } from "../../data-providers/conversation.service";
import { PickFirstConversationCommandService } from "../ui/pick-first-conversation-command.service";

@Injectable({ providedIn: "root" })
export class LoginCommandService {
  constructor(
    private readonly jwtService: JWTService,
    private readonly socket: Socket,
    private readonly router: Router,
    private readonly userService: UserService,
    private readonly conversationService: ConversationService,
    private readonly pickFirstConversationCommandService: PickFirstConversationCommandService
  ) {}

  execute(token: string) {
    this.jwtService.setToken(token);
    this.socket.emit("authenticate", token);

    this.userService.fetch();
    this.conversationService.fetch().then(_ => {
      this.pickFirstConversationCommandService.execute();
    });
    this.userService.loadProfile().subscribe((user: User) => {
      this.userService.setCurrentUser(user);
      this.router.navigate(["/"]);
    });
  }
}
