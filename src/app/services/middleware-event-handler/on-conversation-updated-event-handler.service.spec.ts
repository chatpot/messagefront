import { TestBed } from "@angular/core/testing";

import { OnConversationUpdatedEventHandlerService } from "./on-conversation-updated-event-handler.service";

describe("OnConversationUpdatedEventHandlerService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: OnConversationUpdatedEventHandlerService = TestBed.get(
      OnConversationUpdatedEventHandlerService
    );
    expect(service).toBeTruthy();
  });
});
