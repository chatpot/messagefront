import { Injectable } from "@angular/core";

@Injectable({ providedIn: "root" })
export class JWTService {
  private token: string;
  constructor() {}

  getToken(): string {
    return this.token;
  }

  setToken(token: string): void {
    this.token = token;
  }
}
