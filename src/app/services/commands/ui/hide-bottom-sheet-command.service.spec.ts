import { TestBed } from '@angular/core/testing';

import { HideBottomSheetCommandService } from './hide-bottom-sheet-command.service';

describe('HideBottomSheetCommandService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HideBottomSheetCommandService = TestBed.get(HideBottomSheetCommandService);
    expect(service).toBeTruthy();
  });
});
