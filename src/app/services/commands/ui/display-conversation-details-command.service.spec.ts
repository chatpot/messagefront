import { TestBed } from '@angular/core/testing';

import { DisplayConversationDetailsCommandService } from './display-conversation-details-command.service';

describe('DisplayConversationDetailsCommandService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DisplayConversationDetailsCommandService = TestBed.get(DisplayConversationDetailsCommandService);
    expect(service).toBeTruthy();
  });
});
