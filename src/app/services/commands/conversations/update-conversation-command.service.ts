import {Injectable} from "@angular/core";
import {
  ConversationRemoteProxyService
} from "../../remote-proxies/conversation-remote-proxy.service";
import {
  HideBottomSheetCommandService
} from '../ui/hide-bottom-sheet-command.service';

export interface UpdateConversationCommand {
  name: string;
  members: Array<string>;
  id: string;
  image: string;
}

@Injectable({providedIn: "root"})
export class UpdateConversationCommandService {
  constructor(
      private conversationRemoteProxyService: ConversationRemoteProxyService,
      private hideBottomSheetCommandService: HideBottomSheetCommandService) {}

  execute(updateConversationCommand: UpdateConversationCommand) {
    this.conversationRemoteProxyService.update(updateConversationCommand)
        .subscribe(_ => { this.hideBottomSheetCommandService.execute(); });
  }
}
