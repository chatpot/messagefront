export class RemoteProxyConfiguration {
  userEndpoint: string;
  messageEndpoint: string;
  conversationEndpoint: string;

  constructor(
      userEndpoint: string, messageEndpoint: string,
      conversationEndpoint: string) {
    this.userEndpoint = userEndpoint;
    this.messageEndpoint = messageEndpoint;
    this.conversationEndpoint = conversationEndpoint;
  }
}
