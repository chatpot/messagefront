import { TestBed } from '@angular/core/testing';

import { SendMessageCommandService } from './send-message-command.service';

describe('SendMessageCommandService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SendMessageCommandService = TestBed.get(SendMessageCommandService);
    expect(service).toBeTruthy();
  });
});
