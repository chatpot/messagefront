import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Socket } from "ngx-socket-io";
import { Observable } from "rxjs";
import { RemoteProxyConfiguration } from "./remote-proxy-configuration";

export interface CreateUserRequestDto {
  displayName: string;
  image: string;
}

export interface UserDto {
  id: string;
  displayName: string;
  image: string;
}

@Injectable({ providedIn: "root" })
export class UserRemoteProxyService {
  private url: string;

  private newUserSubject: Observable<UserDto> = this.socket.fromEvent<UserDto>(
    "registered.user"
  );

  private updatedUserSubject: Observable<UserDto> = this.socket.fromEvent<
    UserDto
  >("updated.user");

  private deletedUserSubject: Observable<UserDto> = this.socket.fromEvent<
    UserDto
  >("deleted.user");

  constructor(
    private httpClient: HttpClient,
    private socket: Socket,
    private remoteProxyConfiguration: RemoteProxyConfiguration
  ) {
    this.url = this.remoteProxyConfiguration.userEndpoint;
  }

  getUsers(): Observable<Array<UserDto>> {
    return <Observable<Array<UserDto>>>this.httpClient.get(this.url);
  }

  getNewUserSubject(): Observable<UserDto> {
    return this.newUserSubject;
  }

  getUpdatedUserSubject(): Observable<UserDto> {
    return this.updatedUserSubject;
  }

  getDeletedUserSubject(): Observable<UserDto> {
    return this.deletedUserSubject;
  }

  delete(userId: string): Observable<UserDto> {
    return <Observable<UserDto>>this.httpClient.delete(this.url + userId);
  }

  getUser(id: string): Observable<UserDto> {
    return <Observable<UserDto>>this.httpClient.get(this.url + id);
  }

  refresh(): Observable<UserDto> {
    return <Observable<UserDto>>this.httpClient.put(this.url + "refresh", {});
  }
}
