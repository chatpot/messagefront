import {Injectable} from '@angular/core';
import {MatBottomSheet} from '@angular/material';

@Injectable({providedIn: 'root'})
export class HideBottomSheetCommandService {
  constructor(private matBottomSheet: MatBottomSheet) {}

  execute(): void { this.matBottomSheet.dismiss(); }
}
