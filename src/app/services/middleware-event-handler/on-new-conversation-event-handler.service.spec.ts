import { TestBed } from '@angular/core/testing';

import { OnNewConversationEventHandlerService } from './on-new-conversation-event-handler.service';

describe('OnNewConversationEventHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OnNewConversationEventHandlerService = TestBed.get(OnNewConversationEventHandlerService);
    expect(service).toBeTruthy();
  });
});
