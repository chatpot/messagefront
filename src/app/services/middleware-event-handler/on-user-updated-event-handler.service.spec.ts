import { TestBed } from '@angular/core/testing';

import { OnUserUpdatedEventHandlerService } from './on-user-updated-event-handler.service';

describe('OnUserUpdatedEventHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OnUserUpdatedEventHandlerService = TestBed.get(OnUserUpdatedEventHandlerService);
    expect(service).toBeTruthy();
  });
});
