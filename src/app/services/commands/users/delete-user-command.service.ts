import { Injectable } from "@angular/core";
import {
  UserRemoteProxyService,
  UserDto
} from "../../remote-proxies/user-remote-proxy.service";
import { AuthService } from "../../auth/auth.service";

@Injectable({
  providedIn: "root"
})
export class DeleteUserCommandService {
  constructor(
    private userRemoteProxyService: UserRemoteProxyService,
    private authService: AuthService
  ) {}

  execute(id: string): void {
    this.userRemoteProxyService.delete(id).subscribe((result: UserDto) => {
      this.authService.signOut();
    });
  }
}
