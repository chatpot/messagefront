import { TestBed } from '@angular/core/testing';

import { ConversationRemoteProxyService } from './conversation-remote-proxy.service';

describe('ConversationRemoteProxyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConversationRemoteProxyService = TestBed.get(ConversationRemoteProxyService);
    expect(service).toBeTruthy();
  });
});
