import {Component, OnInit} from "@angular/core";
import {FormControl} from "@angular/forms";
import {
  SendMessageCommandService
} from "src/app/services/commands/messages/send-message-command.service";

@Component({
  selector: "app-send-message",
  templateUrl: "./send-message.component.html",
  styleUrls: ["./send-message.component.less"]
})
export class SendMessageComponent implements OnInit {
  messageControl: FormControl;

  constructor(private sendMessageCommandService: SendMessageCommandService) {}

  ngOnInit() { this.messageControl = new FormControl(); }

  sendMessage() {
    if (!this.messageControl.value) {
      return;
    }

    this.sendMessageCommandService.execute(this.messageControl.value);
    this.messageControl.setValue("");
  }
}
