import {Injectable} from '@angular/core';
import {ConversationService} from '../../data-providers/conversation.service';
import {
  SetCurrentConversationCommandService
} from './set-current-conversation-command.service';

@Injectable({providedIn: 'root'})
export class PickFirstConversationCommandService {
  constructor(
      private readonly conversationService: ConversationService,
      private readonly setCurrentConversationCommandService:
          SetCurrentConversationCommandService) {}
  execute() {
    this.setCurrentConversationCommandService.execute(
        this.conversationService.conversations.find(
            conversation => conversation.messages.length > 0));
  }
}
