import { TestBed } from '@angular/core/testing';

import { LogoutCommandService } from './logout-command.service';

describe('LogoutCommandService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LogoutCommandService = TestBed.get(LogoutCommandService);
    expect(service).toBeTruthy();
  });
});
