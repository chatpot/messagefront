import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ScrollMessageHolderService {
  shouldScrollToBottom: boolean = false;

  constructor() {}

  scrollToBottom() {
    this.shouldScrollToBottom = true;
  }
}
