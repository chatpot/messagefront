import { TestBed } from '@angular/core/testing';

import { OnUserRemovedEventHandlerService } from './on-user-removed-event-handler.service';

describe('OnUserRemovedEventHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OnUserRemovedEventHandlerService = TestBed.get(OnUserRemovedEventHandlerService);
    expect(service).toBeTruthy();
  });
});
