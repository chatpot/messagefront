import {Injectable} from '@angular/core';
import {Conversation} from '../../data-providers/conversation.service';
import {MatBottomSheet} from '@angular/material';
import {
  ConversationDetailsSheetComponent
} from 'src/app/sheets/conversation-details-sheet/conversation-details-sheet.component';

@Injectable({providedIn: 'root'})
export class DisplayConversationDetailsCommandService {
  constructor(private matBottomSheet: MatBottomSheet) {}


  execute(conversation: Conversation): void {
    this.matBottomSheet.open(ConversationDetailsSheetComponent, {
      data: {conversation: conversation},
    });
  }
}
