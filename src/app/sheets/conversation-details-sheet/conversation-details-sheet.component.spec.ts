import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversationDetailsSheetComponent } from './conversation-details-sheet.component';

describe('ConversationDetailsSheetComponent', () => {
  let component: ConversationDetailsSheetComponent;
  let fixture: ComponentFixture<ConversationDetailsSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConversationDetailsSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversationDetailsSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
