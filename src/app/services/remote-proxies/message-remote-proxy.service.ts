import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Socket } from "ngx-socket-io";
import { Observable, Subject } from "rxjs";
import { RemoteProxyConfiguration } from "./remote-proxy-configuration";

export interface MessageDto {
  id: string;
  content: string;
  senderId: string;
  conversationId: string;
  date: Date;
}

export interface CreateMessageRequestDto {
  content: string;
  senderId: string;
  conversationId: string;
}

@Injectable({ providedIn: "root" })
export class MessageRemoteProxyService {
  private url: string;

  private newMessage: Observable<MessageDto> = this.socket.fromEvent<
    MessageDto
  >("sent.message");
  private newMessageSubject: Subject<MessageDto> = new Subject<MessageDto>();

  constructor(
    private httpClient: HttpClient,
    private socket: Socket,
    private remoteProxyConfiguration: RemoteProxyConfiguration
  ) {
    this.url = this.remoteProxyConfiguration.messageEndpoint;
    this.newMessage.subscribe((message: MessageDto) => {
      this.newMessageSubject.next(message);
    });
  }

  getNewMessageSubject(): Observable<MessageDto> {
    return this.newMessageSubject.asObservable();
  }

  create(createMessageRequest: CreateMessageRequestDto): Observable<any> {
    return <Observable<any>>(
      this.httpClient.post(this.url, createMessageRequest)
    );
  }
}
