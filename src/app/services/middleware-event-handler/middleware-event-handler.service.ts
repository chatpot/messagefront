import { Injectable } from "@angular/core";
import { OnConversationRemovedEventHandlerService } from "./on-conversation-removed-event-handler.service";
import { OnConversationUpdatedEventHandlerService } from "./on-conversation-updated-event-handler.service";
import { OnNewConversationEventHandlerService } from "./on-new-conversation-event-handler.service";
import { OnNewMessageEventHandlerService } from "./on-new-message-event-handler.service";
import { OnNewUserEventHandlerService } from "./on-new-user-event-handler.service";
import { OnUserRemovedEventHandlerService } from "./on-user-removed-event-handler.service";
import { OnUserUpdatedEventHandlerService } from "./on-user-updated-event-handler.service";

@Injectable({
  providedIn: "root"
})
export class MiddlewareEventHandlerService {
  constructor(
    private readonly onConversationRemovedEventHandlerService: OnConversationRemovedEventHandlerService,
    private readonly onConversationUpdatedEventHandlerService: OnConversationUpdatedEventHandlerService,
    private readonly onNewConversationEventHandlerService: OnNewConversationEventHandlerService,
    private readonly onNewMessageEventHandlerService: OnNewMessageEventHandlerService,
    private readonly onNewUserEventHandlerService: OnNewUserEventHandlerService,
    private readonly onUserRemovedEventHandlerService: OnUserRemovedEventHandlerService,
    private readonly onUserUpdatedEventHandlerService: OnUserUpdatedEventHandlerService
  ) {}
}
