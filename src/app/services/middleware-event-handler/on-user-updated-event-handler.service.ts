import { Injectable } from "@angular/core";
import { UserService } from "../data-providers/user.service";
import {
  UserRemoteProxyService,
  UserDto
} from "../remote-proxies/user-remote-proxy.service";

@Injectable({
  providedIn: "root"
})
export class OnUserUpdatedEventHandlerService {
  constructor(
    private readonly userRemoteProxyService: UserRemoteProxyService,
    private readonly userService: UserService
  ) {
    this.userRemoteProxyService
      .getUpdatedUserSubject()
      .subscribe((userDto: UserDto) => {
        this.userService.updateUserDto(userDto);
      });
  }
}
