import { TestBed } from '@angular/core/testing';

import { MessageRemoteProxyService } from './message-remote-proxy.service';

describe('MessageRemoteProxyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MessageRemoteProxyService = TestBed.get(MessageRemoteProxyService);
    expect(service).toBeTruthy();
  });
});
