import { Component } from "@angular/core";
import {
  Conversation,
  ConversationService
} from "../../services/data-providers/conversation.service";
import { SetCurrentConversationCommandService } from "src/app/services/commands/ui/set-current-conversation-command.service";

@Component({
  selector: "app-conversations",
  templateUrl: "./conversations.component.html",
  styleUrls: ["./conversations.component.less"]
})
export class ConversationsComponent {
  constructor(
    readonly conversationService: ConversationService,
    private readonly setCurrentConversationCommandService: SetCurrentConversationCommandService
  ) {}

  selectConversation(conversation: Conversation): void {
    this.setCurrentConversationCommandService.execute(conversation);
  }
}
