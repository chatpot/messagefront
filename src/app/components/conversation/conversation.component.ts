import {
  Component,
  ViewChild,
  ElementRef,
  AfterViewChecked,
  DoCheck
} from "@angular/core";
import { ConversationService } from "../../services/data-providers/conversation.service";
import { User, UserService } from "../../services/data-providers/user.service";
import { DisplayConversationDetailsCommandService } from "src/app/services/commands/ui/display-conversation-details-command.service";
import { ScrollMessageHolderService } from "src/app/services/scroll-message-holder.service";

@Component({
  selector: "app-conversation",
  templateUrl: "./conversation.component.html",
  styleUrls: ["./conversation.component.less"]
})
export class ConversationComponent implements DoCheck, AfterViewChecked {
  @ViewChild("messageHolder", { static: true })
  private myScrollContainer: ElementRef;

  constructor(
    readonly conversationService: ConversationService,
    private readonly userService: UserService,
    private readonly displayConversationDetailsCommandService: DisplayConversationDetailsCommandService,
    private readonly scrollMessageHolderService: ScrollMessageHolderService
  ) {}

  ngDoCheck(): void {
    if (this.conversationService.currentConversation)
      this.conversationService.currentConversation.unreadMessage = 0;
  }

  ngAfterViewChecked(): void {
    if (this.scrollMessageHolderService.shouldScrollToBottom) {
      this.scrollToBottom();
      this.scrollMessageHolderService.shouldScrollToBottom = false;
    }
  }

  scrollToBottom(): void {
    if (this.myScrollContainer) {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    }
  }

  isMe(userId: string): boolean {
    return this.userService.currentUser.id === userId;
  }

  getDisplayName(userId: string): string {
    const user = this.userService.users.find(
      (user: User) => user.id === userId
    );
    return user ? user.displayName : "unknown user";
  }

  displayConversation() {
    this.displayConversationDetailsCommandService.execute(
      this.conversationService.currentConversation
    );
  }

  membersIdToUsers(memberIds: Array<string>): Array<User> {
    return this.userService.users.filter((user: User) => {
      return memberIds.includes(user.id);
    });
  }
}
