import {Injectable} from "@angular/core";
import {
  ConversationService,
  Conversation
} from "../../data-providers/conversation.service";
import {ScrollMessageHolderService} from "../../scroll-message-holder.service";

@Injectable({providedIn: "root"})
export class SetCurrentConversationCommandService {
  constructor(
      private readonly conversationService: ConversationService,
      private readonly scrollMessageHolderService: ScrollMessageHolderService) {
  }

  execute(conversation: Conversation) {
    this.conversationService.setCurrentConversation(conversation);
    this.scrollMessageHolderService.scrollToBottom();
  }
}
