import { TestBed } from '@angular/core/testing';

import { UpdateConversationCommandService } from './update-conversation-command.service';

describe('UpdateConversationCommandService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UpdateConversationCommandService = TestBed.get(UpdateConversationCommandService);
    expect(service).toBeTruthy();
  });
});
