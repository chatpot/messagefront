import {Component, OnInit, Inject} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA} from '@angular/material';

@Component({
  selector: 'app-conversation-details-sheet',
  templateUrl: './conversation-details-sheet.component.html',
  styleUrls: ['./conversation-details-sheet.component.less']
})
export class ConversationDetailsSheetComponent implements OnInit {
  constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: any) {}

  ngOnInit() {}
}
