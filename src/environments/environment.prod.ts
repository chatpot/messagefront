import {SocketIoConfig} from "ngx-socket-io";
import {
  RemoteProxyConfiguration
} from "src/app/services/remote-proxies/remote-proxy-configuration";

const socketIoConfig: SocketIoConfig = {
  url: "/events",
  options: {}
};

const remoteProxyConfiguration: RemoteProxyConfiguration = {
  userEndpoint: "/api/users/",
  messageEndpoint: "/api/messages/",
  conversationEndpoint: "/api/conversations/"
};

const firebaseConfig = {
  apiKey: "AIzaSyCbptICT4caxE1-Dl4DnDQO2ZSGmmSZ2Qg",
  authDomain: "chat-po-t.firebaseapp.com",
  databaseURL: "https://chat-po-t.firebaseio.com",
  projectId: "chat-po-t",
  storageBucket: "chat-po-t.appspot.com",
  messagingSenderId: "505881982873",
  appId: "1:505881982873:web:87c5b8c3f0c0a7f4dec508",
  measurementId: "G-WBVM9YWYJE"
};

export const environment = {
  production: true,
  remoteProxyConfiguration: remoteProxyConfiguration,
  socketIoConfig: socketIoConfig,
  firebaseConfig: firebaseConfig
};
