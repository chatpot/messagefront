import { Injectable } from "@angular/core";
import {
  ConversationRemoteProxyService,
  ConversationDto
} from "../remote-proxies/conversation-remote-proxy.service";
import { UserService } from "../data-providers/user.service";
import { ConversationService } from "../data-providers/conversation.service";

@Injectable({
  providedIn: "root"
})
export class OnConversationUpdatedEventHandlerService {
  constructor(
    private readonly conversationRemoteProxyService: ConversationRemoteProxyService,
    private readonly conversationService: ConversationService,
    private readonly userService: UserService
  ) {
    this.conversationRemoteProxyService
      .getUpdatedConversationSubject()
      .subscribe((conversationDto: ConversationDto) => {
        this.updateOrRemoveConversationDtoBasedOnMembership(conversationDto);
      });
  }

  private updateOrRemoveConversationDtoBasedOnMembership(
    conversationDto: ConversationDto
  ): void {
    if (conversationDto.members.includes(this.userService.currentUser.id))
      this.conversationService.updateConversationDto(conversationDto);
    else this.conversationService.removeConversationId(conversationDto.id);
  }
}
