import { Component, OnInit } from "@angular/core";
import { MiddlewareEventHandlerService } from "./services/middleware-event-handler/middleware-event-handler.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.less"]
})
export class AppComponent implements OnInit {
  // TODO: find out better way of doing "array injection"
  constructor(
    private readonly middlewareEventHandlerService: MiddlewareEventHandlerService
  ) {}

  ngOnInit(): void {}
}
