import { Injectable } from "@angular/core";
import {
  ConversationRemoteProxyService,
  ConversationDto
} from "../remote-proxies/conversation-remote-proxy.service";
import { ConversationService } from "../data-providers/conversation.service";

@Injectable({
  providedIn: "root"
})
export class OnConversationRemovedEventHandlerService {
  constructor(
    private readonly conversationRemoteProxyService: ConversationRemoteProxyService,
    private readonly conversationService: ConversationService
  ) {
    this.conversationRemoteProxyService
      .getDeletedConversationSubject()
      .subscribe((conversation: ConversationDto) => {
        this.conversationService.removeConversationId(conversation.id);
      });
  }
}
