import { Injectable } from "@angular/core";
import {
  UserRemoteProxyService,
  UserDto
} from "../remote-proxies/user-remote-proxy.service";
import { UserService } from "../data-providers/user.service";

@Injectable({
  providedIn: "root"
})
export class OnNewUserEventHandlerService {
  constructor(
    private readonly userRemoteProxyService: UserRemoteProxyService,
    private readonly userService: UserService
  ) {
    this.userRemoteProxyService
      .getNewUserSubject()
      .subscribe((userDto: UserDto) => {
        this.userService.addUser(userDto);
      });
  }
}
