import {Injectable} from "@angular/core";
import {
  ConversationRemoteProxyService
} from "../../remote-proxies/conversation-remote-proxy.service";
import {
  HideBottomSheetCommandService
} from '../ui/hide-bottom-sheet-command.service';

@Injectable({providedIn: "root"})
export class QuitConversationCommandService {
  constructor(
      private conversationRemoteProxyService: ConversationRemoteProxyService,
      private hideBottomSheetCommandService: HideBottomSheetCommandService) {}

  execute(conversationId: string) {
    this.conversationRemoteProxyService.leave(conversationId)
        .subscribe(
            (_: string) => { this.hideBottomSheetCommandService.execute(); });
  }
}
