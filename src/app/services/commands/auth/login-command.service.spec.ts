import { TestBed } from '@angular/core/testing';

import { LoginCommandService } from './login-command.service';

describe('LoginCommandService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoginCommandService = TestBed.get(LoginCommandService);
    expect(service).toBeTruthy();
  });
});
