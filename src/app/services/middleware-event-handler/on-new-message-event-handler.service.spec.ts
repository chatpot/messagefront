import { TestBed } from '@angular/core/testing';

import { OnNewMessageEventHandlerService } from './on-new-message-event-handler.service';

describe('OnNewMessageEventHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OnNewMessageEventHandlerService = TestBed.get(OnNewMessageEventHandlerService);
    expect(service).toBeTruthy();
  });
});
