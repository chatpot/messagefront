import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { auth } from "firebase/app";
import { LoginCommandService } from "../commands/auth/login-command.service";
import { LogoutCommandService } from "../commands/auth/logout-command.service";
@Injectable({ providedIn: "root" })
export class AuthService {
  constructor(
    private readonly angularFireAuth: AngularFireAuth,
    private readonly loginCommandService: LoginCommandService,
    private readonly logoutCommandService: LogoutCommandService
  ) {
    this.angularFireAuth.authState.subscribe((user: firebase.User) => {
      this.authStateChanged(user);
    });
  }

  private authStateChanged(user: firebase.User): void {
    if (user) {
      user
        .getIdToken()
        .then((token: string) => {
          this.loginCommandService.execute(token);
        })
        .catch(reason => {});
    } else {
      this.logoutCommandService.execute();
    }
  }

  loginWithGoogle() {
    const googleProvider = new auth.GoogleAuthProvider();
    this.loginWithProvider(googleProvider);
  }

  signOut() {
    this.angularFireAuth.auth
      .signOut()
      .then(() => {})
      .catch(reason => {});
  }

  private loginWithProvider(provider: firebase.auth.AuthProvider) {
    this.angularFireAuth.auth
      .signInWithPopup(provider)
      .then(() => {})
      .catch(reason => {});
  }
}
