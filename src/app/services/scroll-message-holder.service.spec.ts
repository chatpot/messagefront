import { TestBed } from '@angular/core/testing';

import { ScrollMessageHolderService } from './scroll-message-holder.service';

describe('ScrollMessageHolderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScrollMessageHolderService = TestBed.get(ScrollMessageHolderService);
    expect(service).toBeTruthy();
  });
});
