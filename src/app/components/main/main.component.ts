import { Component, OnInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { MediaMatcher } from "@angular/cdk/layout";
import {
  User,
  UserService
} from "src/app/services/data-providers/user.service";
import { Subscription } from "rxjs";
import { DisplayUserDetailsCommandService } from "src/app/services/commands/ui/display-user-details-command.service";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.less"]
})
export class MainComponent implements OnInit, OnDestroy {
  title = "MessageFront";
  mobileQuery: MediaQueryList;
  tabletQuery: MediaQueryList;

  private _mobileQueryListener: () => void;

  constructor(
    readonly userService: UserService,
    private changeDetectorRef: ChangeDetectorRef,
    private media: MediaMatcher,
    private displayUserDetailsCommandService: DisplayUserDetailsCommandService
  ) {}

  ngOnInit(): void {
    this.mobileQuery = this.media.matchMedia("(max-width: 600px)");
    this.tabletQuery = this.media.matchMedia("(max-width: 800px)");

    this._mobileQueryListener = () => this.changeDetectorRef.detectChanges();

    this.mobileQuery.addListener(this._mobileQueryListener);
    this.tabletQuery.addListener(this._mobileQueryListener);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
    this.tabletQuery.removeListener(this._mobileQueryListener);
  }

  getSidenavClass(): string {
    if (this.mobileQuery.matches) return "sidenav-mobile";
    else if (this.tabletQuery.matches) return "sidenav-tablet";
    else return "sidenav";
  }

  displayUser() {
    this.displayUserDetailsCommandService.execute(this.userService.currentUser);
  }
}
