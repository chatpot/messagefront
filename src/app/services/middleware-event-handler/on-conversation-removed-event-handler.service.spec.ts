import { TestBed } from '@angular/core/testing';

import { OnConversationRemovedEventHandlerService } from './on-conversation-removed-event-handler.service';

describe('OnConversationRemovedEventHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OnConversationRemovedEventHandlerService = TestBed.get(OnConversationRemovedEventHandlerService);
    expect(service).toBeTruthy();
  });
});
