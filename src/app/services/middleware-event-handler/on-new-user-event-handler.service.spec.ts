import { TestBed } from '@angular/core/testing';

import { OnNewUserEventHandlerService } from './on-new-user-event-handler.service';

describe('OnNewUserEventHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OnNewUserEventHandlerService = TestBed.get(OnNewUserEventHandlerService);
    expect(service).toBeTruthy();
  });
});
