import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatListModule} from "@angular/material/list";
import {MatIconModule} from "@angular/material/icon";
import {MatFormFieldModule} from "@angular/material/form-field";
import {
  MatInputModule,
  MatSidenavModule,
  MatExpansionModule,
  MatBadgeModule,
  MatTabsModule,
  MatBottomSheetModule
} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {
  ConversationComponent
} from "./components/conversation/conversation.component";
import {
  SendMessageComponent
} from "./components/send-message/send-message.component";
import {
  CreateConversationComponent
} from "./components/create-conversation/create-conversation.component";
import {LoginComponent} from "./components/login/login.component";
import {SocketIoModule} from "ngx-socket-io";
import {UsersComponent} from "./components/users/users.component";
import {
  ConversationsComponent
} from "./components/conversations/conversations.component";
import {
  ConversationDetailsComponent
} from "./components/conversation-details/conversation-details.component";
import {
  UserDetailsComponent
} from "./components/user-details/user-details.component";
import {
  RemoteProxyConfiguration
} from "./services/remote-proxies/remote-proxy-configuration";
import {environment} from "src/environments/environment";
import {MainComponent} from "./components/main/main.component";
import {Routes, RouterModule} from "@angular/router";
import {LoginGuard} from "./services/auth/login.guard";
import {AngularFireModule} from "@angular/fire";
import {AngularFireAuthModule} from "@angular/fire/auth";
import {JWTInterceptor} from "./services/auth/jwt.interceptor";
import {
  UserDetailsSheetComponent
} from './sheets/user-details-sheet/user-details-sheet.component';
import {
  ConversationDetailsSheetComponent
} from './sheets/conversation-details-sheet/conversation-details-sheet.component';

const appRoutes: Routes = [
  {path: "login", component: LoginComponent},
  {path: "", component: MainComponent, canActivate: [LoginGuard]},
  {path: "**", redirectTo: ""}
];

@NgModule({
  declarations: [
    AppComponent, ConversationComponent, SendMessageComponent,
    CreateConversationComponent, LoginComponent, MainComponent, UsersComponent,
    ConversationsComponent, ConversationDetailsComponent, UserDetailsComponent,
    UserDetailsSheetComponent, ConversationDetailsSheetComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatGridListModule,
    MatListModule,
    MatIconModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatExpansionModule,
    MatBadgeModule,
    MatInputModule,
    MatTabsModule,
    MatBottomSheetModule,
    FormsModule,
    ReactiveFormsModule,
    SocketIoModule.forRoot(environment.socketIoConfig),
    RouterModule.forRoot(appRoutes, {}),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule
  ],
  entryComponents:
      [UserDetailsSheetComponent, ConversationDetailsSheetComponent],
  providers: [
    {
      provide: RemoteProxyConfiguration,
      useValue: environment.remoteProxyConfiguration
    },
    {provide: HTTP_INTERCEPTORS, useClass: JWTInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
