import { TestBed } from '@angular/core/testing';

import { SetCurrentConversationCommandService } from './set-current-conversation-command.service';

describe('SetCurrentConversationCommandService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SetCurrentConversationCommandService = TestBed.get(SetCurrentConversationCommandService);
    expect(service).toBeTruthy();
  });
});
