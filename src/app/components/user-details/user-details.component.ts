import {Component, Input} from "@angular/core";
import {User, UserService} from "../../services/data-providers/user.service";
import {
  DeleteUserCommandService
} from "src/app/services/commands/users/delete-user-command.service";
import {AuthService} from "src/app/services/auth/auth.service";

@Component({
  selector: "app-user-details",
  templateUrl: "./user-details.component.html",
  styleUrls: ["./user-details.component.less"]
})
export class UserDetailsComponent {
  @Input() user: User;

  constructor(
      private userService: UserService,
      private deleteUserCommandService: DeleteUserCommandService,
      private authService: AuthService) {}


  isMe() {
    return this.userService.currentUser &&
        this.user.id === this.userService.currentUser.id;
  }

  logout() { this.authService.signOut(); }

  delete () { this.deleteUserCommandService.execute(this.user.id); }
}
